package br.com.thiago.microservices.loja.dto;

import java.io.Serializable;

public class InfoFornecedorDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String endereco;

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

}
