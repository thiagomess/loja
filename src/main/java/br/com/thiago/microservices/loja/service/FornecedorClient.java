package br.com.thiago.microservices.loja.service;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import br.com.thiago.microservices.loja.dto.InfoFornecedorDTO;
import br.com.thiago.microservices.loja.dto.InfoPedidoDTO;
import br.com.thiago.microservices.loja.dto.ItemDaCompraDTO;

@FeignClient(name = "fornecedor") //ID da aplicação que esta definido no eureka que o feign irá acessar
//@FeignClient(url = "https://viacep.com.br/ws/", name = "viacep") //Se fosse acessar uma URL, seria assim
public interface FornecedorClient {

	@GetMapping("/info/{estado}")
	public InfoFornecedorDTO getInfoPorEstado(@PathVariable String estado);

	@PostMapping("/pedido")
	InfoPedidoDTO realizaPedido(List<ItemDaCompraDTO> list);
}
