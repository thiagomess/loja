package br.com.thiago.microservices.loja.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.web.client.RestTemplate;

import feign.RequestInterceptor;
import feign.RequestTemplate;

@Configuration
public class Configuracao {

	/*
	 * A aplicação cliente conhece as instâncias disponíveis e, usando algum
	 * algoritmo de rotação do Ribbon, acessa uma instância diferente a cada
	 * requisição do usuário
	 * https://cloud.spring.io/spring-cloud-static/spring-cloud-netflix/2.2.0.RELEASE/reference/html/#spring-cloud-ribbon
	 */
	@Bean
	@LoadBalanced //da a inteligencia para o RestTemplate conseguir achar o 
	//application.name registrado no eureka
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}
	
	
	
	@Bean
	public RequestInterceptor getInterceptorAutenticacao() {
		
		return new RequestInterceptor() {
			@Override
			public void apply(RequestTemplate template) {
				Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
				if (authentication == null) {
					return;
				}
				OAuth2AuthenticationDetails details = (OAuth2AuthenticationDetails) authentication.getDetails();
				template.header("Authorization", "Bearer " + details.getTokenValue());
			}
		};
	}
}
