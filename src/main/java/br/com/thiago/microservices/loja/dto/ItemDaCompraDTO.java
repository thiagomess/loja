package br.com.thiago.microservices.loja.dto;

import java.io.Serializable;

public class ItemDaCompraDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private Long id;
	private Integer quantidade;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

}
