package br.com.thiago.microservices.loja.dto;

import java.io.Serializable;

public class EnderecoDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String rua;

	private Integer numero;

	private String estado;

	private String cidade;

	public String getRua() {
		return rua;
	}

	public void setRua(String rua) {
		this.rua = rua;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	@Override
	public String toString() {
		return "EnderecoDTO [rua=" + rua + ", numero=" + numero + ", estado=" + estado + ", cidade=" + cidade + "]";
	}
	

}
