package br.com.thiago.microservices.loja.service.exceptions;

public class ConexaoExternaException extends RuntimeException {

	private static final long serialVersionUID = -4680762495158048958L;

    public ConexaoExternaException(String msg){
        super(msg);
    }

    public ConexaoExternaException(String msg, Throwable cause){
        super(msg,cause);
    }

}