package br.com.thiago.microservices.loja.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.thiago.microservices.loja.dto.CompraDTO;
import br.com.thiago.microservices.loja.model.Compra;
import br.com.thiago.microservices.loja.service.CompraService;
import br.com.thiago.microservices.loja.service.OrquestradorDeCompraService;

@RestController
@RequestMapping("/compra")
public class CompraController {

	@Autowired
	private CompraService compraService;
	
	@Autowired
	private OrquestradorDeCompraService orquestradorDeCompraService;
	
	

	@PostMapping
	public Compra realizaCompra(@RequestBody CompraDTO compra) {
		Compra obj = orquestradorDeCompraService.realizaCompra(compra);
		return obj;
	}

	@GetMapping(value = "/{id}")
	public Compra getById(@PathVariable Long id) {

		return compraService.getById(id);
	}

}
