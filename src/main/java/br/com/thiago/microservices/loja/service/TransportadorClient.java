package br.com.thiago.microservices.loja.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

import br.com.thiago.microservices.loja.dto.InfoEntregaDTO;
import br.com.thiago.microservices.loja.dto.VoucherDTO;

@FeignClient(name = "transportador") //ID da aplicação que esta definido no eureka que o feign irá acessar
//@FeignClient(url = "https://viacep.com.br/ws/", name = "viacep") //Se fosse acessar uma URL, seria assim
public interface TransportadorClient {

	
	@PostMapping("/entrega")
	VoucherDTO geraVoucher(InfoEntregaDTO entrega);
}
