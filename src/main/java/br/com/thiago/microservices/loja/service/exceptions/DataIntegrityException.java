package br.com.thiago.microservices.loja.service.exceptions;

public class DataIntegrityException extends RuntimeException {

	private static final long serialVersionUID = -4680762495158048958L;

    public DataIntegrityException(String msg){
        super(msg);
    }

    public DataIntegrityException(String msg, Throwable cause){
        super(msg,cause);
    }

}