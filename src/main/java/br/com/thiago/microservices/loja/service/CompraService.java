package br.com.thiago.microservices.loja.service;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

import br.com.thiago.microservices.loja.dto.CompraDTO;
import br.com.thiago.microservices.loja.dto.InfoEntregaDTO;
import br.com.thiago.microservices.loja.dto.InfoFornecedorDTO;
import br.com.thiago.microservices.loja.dto.InfoPedidoDTO;
import br.com.thiago.microservices.loja.dto.VoucherDTO;
import br.com.thiago.microservices.loja.model.Compra;
import br.com.thiago.microservices.loja.model.CompraEstado;
import br.com.thiago.microservices.loja.repository.CompraRepository;
import br.com.thiago.microservices.loja.service.exceptions.ObjectNotFoundException;

@Service
public class CompraService {

	private static final Logger LOG = LoggerFactory.getLogger(CompraService.class);

//	@Autowired
//	private RestTemplate client;
//	
//	@Autowired
//	private DiscoveryClient eurekaClient;

	@Autowired
	private FornecedorClient fornecedorClient;

	@Autowired
	private CompraRepository compraRepository;
	
	@Autowired
	private TransportadorClient transportadorClient;
	
	@HystrixCommand(fallbackMethod = "realizaCompraFallback", threadPoolKey = "reprocessaCompraThreadPool") 
	public Compra reprocessaCompra(CompraDTO compraDto) {
		
		Compra compra = getById(compraDto.getCompraId());
		
		if (compra.getEstado().equals(CompraEstado.RECEBIDO)) {
			LOG.info("Reprocessando o pedido..");
			LOG.info("Iniciando a realização do pedido...");
			realizaPedido(compraDto, compra);
			
			LOG.info("Iniciando a realização da reserva...");
			realizaReserva(compraDto, compra);
			
		}else if (compra.getEstado().equals(CompraEstado.PEDIDO_REALIZADO)) {
			LOG.info("Reprocessando a reserva..");
			
			LOG.info("Iniciando a realização da reserva...");
			realizaReserva(compraDto, compra);
		}
		
		return compra;
	}
	
	public void cancelaCompra(CompraDTO compraDto) {
		Compra compra = getById(compraDto.getCompraId());
		compraRepository.deleteById(compra.getId());
		}

	/* Forma Utilizando o SPRING FEIGN */
	// Ativa o Circuit Breaker
	//fallbackMethod, define o metodo que irá retornar em caso de falha
	//threadPoolKey = ativa o Bulkhead, onde divide as threads entre os metodos, 
	//para evitar falha de todos os metodos em caso de esgotamento de thread
	@HystrixCommand(fallbackMethod = "realizaCompraFallback", threadPoolKey = "realizaCompraThreadPool") 
	public Compra realizaCompra(CompraDTO compraDto) {
		Compra compra = new Compra(compraDto);
		compraRepository.save(compra);
		compraDto.setCompraId(compra.getId());
		LOG.info("Iniciando a realização do pedido...");
		realizaPedido(compraDto, compra);
		
		LOG.info("Iniciando a realização da reserva...");
		realizaReserva(compraDto, compra);
		
		return compra;
	}
	@HystrixCommand 
	private void realizaReserva(CompraDTO compraDto, Compra compra) {
		String estado = compraDto.getEndereco().getEstado();
		LOG.info("Buscando informações do fornecedor de {} ", estado);
		InfoFornecedorDTO fornecedor = fornecedorClient.getInfoPorEstado(estado);
		
		LOG.info("realizando a geração do Voucher");
		InfoEntregaDTO entrega = new InfoEntregaDTO(compra, fornecedor);
		VoucherDTO voucher = transportadorClient.geraVoucher(entrega);
		compra.setNumeroVoucherEntrega(voucher.getNumero());
		compra.setPrevisaoParaEntrega(voucher.getPrevisaoParaEntrega());
		compra.setEstado(CompraEstado.RESERVA_ENTREGA_REALIZADA);
		compraRepository.save(compra);
	}
	
	@HystrixCommand 
	private void realizaPedido(CompraDTO compraDto, Compra compra) {
		LOG.info("Realizando o pedido");
		InfoPedidoDTO pedido = fornecedorClient.realizaPedido(compraDto.getItens());
		compra.setPedidoId(pedido.getId());
		compra.setValorTotal(pedido.getValorTotal());
		compra.setTempoDePreparo(pedido.getTempoDePreparo());
		compra.setEstado(CompraEstado.PEDIDO_REALIZADO);
		compraRepository.save(compra);
	}
	
	@HystrixCommand(threadPoolKey = "getByIdThreadPool")
	public Compra getById(Long id) {
		if (id == null) {
			return null;
		}
		final Optional<Compra> pedido = compraRepository.findById(id);
		return pedido.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto Não encontrado: Id: " + id + " ,tipo: " + Compra.class.getName(), null));
	}

	// Em caso de Falha, o fallback, irá chamar este metodo
	public Compra realizaCompraFallback(CompraDTO compraDto) {
		
		if (compraDto.getCompraId() != null) {
			return compraRepository.findById(compraDto.getCompraId()).get();
		}
		
		Compra compraFallback = new Compra();
		compraFallback.setEnderecoDestino(compraDto.getEndereco().toString());
		return compraFallback;
	}




	/* Forma Utilizando o REST TEMPLATE */
//	public void realizaCompra(CompraDTO compra) {
//
//		//RestTemplate agora consegue capturar a url como fornecedor, pois esta registrada no eureka
//		ResponseEntity<InfoFornecedorDTO> exchange = client.exchange(
//				"http://fornecedor/info/" + compra.getEndereco().getEstado(), HttpMethod.GET, null,
//				InfoFornecedorDTO.class);
//		//Implementação so pra visualizar as portas que o Ribbon está vendo
//		eurekaClient.getInstances("fornecedor").stream()
//					.forEach(fornecedor -> System.out.println(fornecedor.getPort()));
//		//fim
//		System.out.println(exchange.getBody().getEndereco());
//	}

}
