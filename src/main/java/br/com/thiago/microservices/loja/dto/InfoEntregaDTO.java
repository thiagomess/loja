package br.com.thiago.microservices.loja.dto;

import java.time.LocalDate;

import br.com.thiago.microservices.loja.model.Compra;

public class InfoEntregaDTO {

	private Long pedidoId;
	
	private LocalDate dataParaEntrega;
	
	private String enderecoOrigem;
	
	private String enderecoDestino;

	public InfoEntregaDTO(Compra compra, InfoFornecedorDTO fornecedor) {
		pedidoId = compra.getPedidoId();
		dataParaEntrega = LocalDate.now().plusDays(compra.getTempoDePreparo()); 
		enderecoOrigem = fornecedor.getEndereco();
		enderecoDestino = compra.getEnderecoDestino();
	}

	public Long getPedidoId() {
		return pedidoId;
	}

	public void setPedidoId(Long pedidoId) {
		this.pedidoId = pedidoId;
	}

	public LocalDate getDataParaEntrega() {
		return dataParaEntrega;
	}

	public void setDataParaEntrega(LocalDate dataParaEntrega) {
		this.dataParaEntrega = dataParaEntrega;
	}

	public String getEnderecoDestino() {
		return enderecoDestino;
	}

	public void setEnderecoDestino(String enderecoDestino) {
		this.enderecoDestino = enderecoDestino;
	}

	public String getEnderecoOrigem() {
		return enderecoOrigem;
	}

	public void setEnderecoOrigem(String enderecoOrigem) {
		this.enderecoOrigem = enderecoOrigem;
	}
	
}
