package br.com.thiago.microservices.loja.model;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import br.com.thiago.microservices.loja.dto.CompraDTO;

@Entity
public class Compra {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private Long pedidoId;

	private Integer tempoDePreparo;

	private Double valorTotal;

	private String enderecoDestino;
	
	private Long numeroVoucherEntrega;
	
	private LocalDate previsaoParaEntrega;
	
	@Enumerated(EnumType.STRING)
	private CompraEstado estado;

	public Compra(CompraDTO compra) {
		enderecoDestino = compra.getEndereco().toString();
		estado = CompraEstado.RECEBIDO;
	}

	
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Long getPedidoId() {
		return pedidoId;
	}


	public void setPedidoId(Long pedidoId) {
		this.pedidoId = pedidoId;
	}


	public Integer getTempoDePreparo() {
		return tempoDePreparo;
	}


	public void setTempoDePreparo(Integer tempoDePreparo) {
		this.tempoDePreparo = tempoDePreparo;
	}


	public Double getValorTotal() {
		return valorTotal;
	}


	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}


	public String getEnderecoDestino() {
		return enderecoDestino;
	}


	public void setEnderecoDestino(String enderecoDestino) {
		this.enderecoDestino = enderecoDestino;
	}


	public Long getNumeroVoucherEntrega() {
		return numeroVoucherEntrega;
	}


	public void setNumeroVoucherEntrega(Long numeroVoucherEntrega) {
		this.numeroVoucherEntrega = numeroVoucherEntrega;
	}


	public LocalDate getPrevisaoParaEntrega() {
		return previsaoParaEntrega;
	}


	public void setPrevisaoParaEntrega(LocalDate previsaoParaEntrega) {
		this.previsaoParaEntrega = previsaoParaEntrega;
	}


	public CompraEstado getEstado() {
		return estado;
	}


	public void setEstado(CompraEstado estado) {
		this.estado = estado;
	}


	public Compra() {
	}



}
