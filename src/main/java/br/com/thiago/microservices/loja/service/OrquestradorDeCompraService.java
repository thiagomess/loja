package br.com.thiago.microservices.loja.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.thiago.microservices.loja.dto.CompraDTO;
import br.com.thiago.microservices.loja.model.Compra;
import br.com.thiago.microservices.loja.model.CompraEstado;
import br.com.thiago.microservices.loja.service.exceptions.ConexaoExternaException;

@Service
public class OrquestradorDeCompraService {

	@Autowired
	private CompraService compraService;

	public Compra realizaCompra(CompraDTO compraDto) {

		Compra compra = compraService.realizaCompra(compraDto);

		//Caso o realiza Compra nao tenha funcionado de primeira, 
		//ele tenta reprocessar a compra por mais 3 vezes, caso contrario, cancela o pedido
		if (compra.getEstado() != null && !compra.getEstado().equals(CompraEstado.RESERVA_ENTREGA_REALIZADA)) {

			int cont = 0;
			while (!compra.getEstado().equals(CompraEstado.RESERVA_ENTREGA_REALIZADA)) {
				compraService.reprocessaCompra(compraDto);
				compra = compraService.getById(compraDto.getCompraId());
				cont++;

				if (cont == 3) {
					compraService.cancelaCompra(compraDto);
					throw new ConexaoExternaException(
							"Falha na geração do pedido, tente novamente em alguns instantes");
				}
			}
		}

		return compra;
	}

}
